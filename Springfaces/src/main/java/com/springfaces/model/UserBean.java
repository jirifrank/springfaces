package com.springfaces.model;

import java.io.Serializable;

import javax.annotation.PostConstruct;

import org.springframework.context.annotation.Scope;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;

@Controller("userBean")
@Scope("view")
public class UserBean implements Serializable{
	
	private static final long serialVersionUID = 3362168518993055388L;
	
	private Authentication authentication;
	
	@PostConstruct
	public void init(){
		authentication = SecurityContextHolder.getContext().getAuthentication();	
	}
	
	public String getName(){
		return authentication.getName();
	}
	
}
