package com.springfaces.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {
	
	@Autowired
	private AuthenticationProvider authentificationProvider;
	
	@Autowired
	public void configureGlobal(AuthenticationManagerBuilder authManager) throws Exception {
		authManager.authenticationProvider(authentificationProvider);
	}
	
	@Override
	public void configure(WebSecurity web) throws Exception {
		super.configure(web);
	    web
        .ignoring()
            .antMatchers("/resources/**")
            .antMatchers("/jsf/public/**", "/jsf/public");
	    
	}
	
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		super.configure(http);
		http
		.authorizeRequests()
			.antMatchers("/jsf/public/**").permitAll()
			.antMatchers("/jsf/private/**").hasRole("ADMIN")
			.and()
        .formLogin()
            .defaultSuccessUrl("/jsf/private/index.xhtml", true)
            .permitAll() 
            .and()
        .logout()
        	.logoutUrl("/logout")
        	.permitAll()
            .logoutSuccessUrl("/login")
            .and()
        .rememberMe()
            .and()
        .csrf()
        	.disable();

		super.configure(http);
          
	}
	
}
