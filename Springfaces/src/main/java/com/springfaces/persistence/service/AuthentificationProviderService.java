package com.springfaces.persistence.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.stereotype.Service;

import com.springfaces.persistence.entity.User;

@Service
public class AuthentificationProviderService implements AuthenticationProvider {

	@Autowired
	private UserService userService;

	public Authentication authenticate(Authentication authentication) throws AuthenticationException {
		String username = String.valueOf(authentication.getPrincipal());
		String password = String.valueOf(authentication.getCredentials());

		User user = userService.login(username, password);

		if (user == null) {
			throw new BadCredentialsException("Bad username/password!");
		}

		List<GrantedAuthority> authorityList = AuthorityUtils.createAuthorityList("ADMIN");

		return new UsernamePasswordAuthenticationToken(user, null, authorityList);
	}

	public boolean supports(Class<?> authentication) {
		return true;
	}
}