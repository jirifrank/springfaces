package com.springfaces.persistence.service;

import com.springfaces.persistence.entity.User;

public interface UserService {	
	public User login(String username, String password);
}
