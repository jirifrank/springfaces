package com.springfaces.persistence.service;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.springfaces.persistence.dao.UserDAO;
import com.springfaces.persistence.entity.User;
import com.springfaces.utils.Encryption;

@Service
public class UserServiceImpl implements UserService{
		
	@Autowired
	private UserDAO userDAO;
	
	// -- REMOVE ---
	@PostConstruct
	public void init(){
		if(login("admin","admin") == null){
			User user = new User();
			user.setUsername("admin");
			user.setPassword(Encryption.md5Encrypt("admin"));
			userDAO.makePersistent(user);
		}
	}
	
	public User login(String username, String password) {		
		String passwordEncrypted = Encryption.md5Encrypt(password);
		User user = userDAO.findByUsernameAndPassword(username, passwordEncrypted);
		return user;
	}

}
