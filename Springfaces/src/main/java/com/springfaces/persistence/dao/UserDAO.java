package com.springfaces.persistence.dao;

import com.springfaces.persistence.entity.User;

public interface UserDAO extends GenericDAO<User, Integer>{
	User findByUsernameAndPassword(String username, String password);
}
