package com.springfaces.persistence.dao;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Criterion;
import org.springframework.beans.factory.annotation.Autowired;

public abstract class HibernateGenericDAO<T, ID extends Serializable> implements GenericDAO<T, ID> {

    private final Class<T> persistentClass;

    @Autowired
    private SessionFactory sessionFactory;
    private Session session;

    @SuppressWarnings("unchecked")
	public HibernateGenericDAO() {
        this.persistentClass = (Class<T>) ((ParameterizedType) getClass()
                .getGenericSuperclass()).getActualTypeArguments()[0];
    }

    protected Session getSession() {
    	if(session == null)
    		session = sessionFactory.openSession();
    	
    	return session;
    }

    public Class<T> getPersistentClass() {
        return persistentClass;
    }

    @SuppressWarnings("unchecked")
    public T findById(ID id) {
        return (T) getSession().load(getPersistentClass(), id);
    }

    public List<T> findAll() {
        return findByCriteria();
    }

    public T makePersistent(T entity) {
        getSession().beginTransaction();
        getSession().saveOrUpdate(entity);
        getSession().getTransaction().commit();
        return entity;
    }

    public void makeTransient(T entity) {
        getSession().beginTransaction();
        getSession().delete(entity);
        getSession().getTransaction().commit();
    }

    public void flush() {
        getSession().flush();
    }

    public void clear() {
        getSession().clear();
    }

    @SuppressWarnings("unchecked")
	protected List<T> findByCriteria(Criterion... criterion) {
        Criteria crit = getSession().createCriteria(getPersistentClass());
        for (Criterion c : criterion) {
            crit.add(c);
        }
        return crit.list();
    }
    
    @SuppressWarnings("unchecked")
	public T findFirst(){
        Criteria crit = getSession().createCriteria(getPersistentClass());
        crit.setMaxResults(1);
        return (T) crit.uniqueResult();
    }

}
