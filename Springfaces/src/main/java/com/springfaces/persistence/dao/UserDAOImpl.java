package com.springfaces.persistence.dao;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.springfaces.persistence.entity.User;

@Repository
public class UserDAOImpl extends HibernateGenericDAO<User, Integer> implements UserDAO {

	public User findByUsernameAndPassword(String username, String password) {
		Criteria criteria = getSession().createCriteria(User.class);
		criteria.add(Restrictions.and(Restrictions.eq("username", username), Restrictions.eq("password", password)));

		return (User) criteria.uniqueResult();
	}

}
