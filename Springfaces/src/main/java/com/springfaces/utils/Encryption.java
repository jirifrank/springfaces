package com.springfaces.utils;

import org.springframework.security.authentication.encoding.Md5PasswordEncoder;

public class Encryption {

	private static final String SALT = "lH4v6uPTcfnU0EVDkWEg";
	
	public static String md5Encrypt(String password) {
		Md5PasswordEncoder encoder = new Md5PasswordEncoder();

		return encoder.encodePassword(password, SALT);
	}
}
